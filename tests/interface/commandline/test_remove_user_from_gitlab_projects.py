from subprocess import Popen, PIPE


def test_remove_user_from_gitlab_projects_is_available_from_command_line():
    p = Popen(["gllm", "remove-user-from-gitlab-projects", "--help"], stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate()
    assert "usage: gllm" in stdout.decode('utf-8')
