import argparse
import sys

from gllm.commands import (
    get_gitlab_projects,
    remove_user_from_gitlab_projects
)


def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    sp = subparsers.add_parser('get-gitlab-projects')
    get_gitlab_projects.configureArgparser(sp)

    sp = subparsers.add_parser('remove-user-from-gitlab-projects')
    remove_user_from_gitlab_projects.configureArgparser(sp)

    args = parser.parse_args()
    if len(sys.argv) > 1:
        args.func(args)
    else:
        parser.print_help()
